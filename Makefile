USERNAME= wingyu
PROJECT = docker-redis
TAG = 1.0
.PHONY: build, run

build:
	docker build --rm -t $(USERNAME)/$(PROJECT):$(TAG) .

run:
	sudo docker run -p 6379:6379 $(USERNAME)/$(PROJECT):$(TAG)

